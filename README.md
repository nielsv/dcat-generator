# DCAT generator
a simple script that generates a dcat catalog for RDF datasets. Configurable via a yaml file.
The assumption here is each dataset is available as a separate graph in the SPARQL endpoint. This script will query the endpoint to generate void statistics for every dataset.

## running it
```
$ ruby bin/dcat -h
Usage: bin/dcat [OPTIONS]

Options
    -c, --config FILE                location of the config file
    -o, --output OUTPUT_PATH         filepath to store output
    -h, --help                       help
```

An [example config file](dcat-desc.yml) is available in this repository.
