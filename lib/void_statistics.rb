class VoidStatistics
  attr_reader :client

  def initialize(client, graph)
    @graph = graph
    @client = client
  end

  def classes
    begin
      @classes ||= client.query("SELECT (count(distinct(?class)) as ?classes) FROM <#{@graph}> WHERE { ?s a ?class }").first["classes"].value.to_i
    rescue SPARQL::Client::ServerError => e
      puts e
      0
    end
  end

  def subjects
    begin
      @subjects ||= client.query("SELECT (count(distinct(?s)) as ?subjects) FROM <#{@graph}> WHERE { ?s ?p ?o }").first["subjects"].value.to_i
    rescue SPARQL::Client::ServerError => e
      puts e
      0
    end
  end

  def objects
    begin
      @objects ||= client.query("SELECT (count(distinct(?o)) as ?objects) FROM <#{@graph}> WHERE { ?s ?p ?o }").first["objects"].value.to_i
    rescue SPARQL::Client::ServerError => e
      puts e
      0
    end
  end

  def properties
    begin
      @properties ||= client.query("SELECT (count(distinct(?p)) as ?properties) FROM <#{@graph}>  WHERE { ?s ?p ?o }").first["properties"].value.to_i
    rescue SPARQL::Client::ServerError => e
      puts e
      0
    end
  end

  def triples
    begin
      @triples ||= client.query("SELECT (count(*) as ?triples) FROM <#{@graph}> WHERE { ?s ?p ?o }").first["triples"].value.to_i
    rescue SPARQL::Client::ServerError => e
      puts e
      0
    end
  end

  def last_modified
    return @modified if @modified
    result = client.query("SELECT ?modified FROM <#{@graph}> WHERE { ?s <#{RDF::Vocab::DC.modified}> ?modified.} ORDER BY DESC(?modified) LIMIT 1")
    if result.first
      result.first["modified"]
    else
      RDF::Literal::DateTime.new(DateTime.now)
    end
  end
end
