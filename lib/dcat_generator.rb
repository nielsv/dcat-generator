require_relative './void_statistics.rb'

class DcatGenerator
  attr_reader :endpoint, :client, :repo, :catalog_iri, :datasets, :catalog

  def initialize(config)
    @endpoint = config["endpoint"]
    @client = SPARQL::Client.new(endpoint)
    @repo = RDF::Repository.new
    @catalog_iri = RDF::URI.new(config["catalog"]["iri"])
    @catalog = config["catalog"]
    @datasets = config["datasets"]
  end

  def generate_feed(filename)
    generate_catalog
    generate_datasets
    write_feed(filename)
  end

  def write_feed(filename)
    FileUtils.touch(filename)
    File.open(filename, "w") do |file|
      file.write repo.dump(:ntriples)
    end
  end

  def to_data_url(iri)
    RDF::URI.new(iri.value.gsub('/id/','/data/'))
  end

  def insert_triples(triples)
    triples.each do |t|
      repo << t
    end
  end

  def generate_catalog
    catalog_data_url = to_data_url(catalog_iri)
    repo << [catalog_iri, RDF.type, RDF::Vocab::DCAT.Catalog]
    insert_triples language_tagged_literals(catalog_iri, RDF::Vocab::DC.title, catalog["title"])
    insert_triples language_tagged_literals(catalog_iri, RDF::Vocab::DC.description, catalog["description"])
    repo << [catalog_iri, RDF::Vocab::DC.license, RDF::URI.new(catalog["license"])] if catalog["license"]
    repo << [catalog_iri, RDF::Vocab::DC.publisher, RDF::URI.new(catalog["publisher"])] if catalog["publisher"]
    if catalog["language"] && catalog["language"].kind_of?(Array)
      catalog["language"].each do |lang|
        repo << [catalog_iri, RDF::Vocab::DC.language, RDF::Literal.new(lang)]
      end
    end
    repo << [catalog_iri, RDF::Vocab::DC.modified, now]
    repo << [catalog_iri, RDF::Vocab::RDFS.isDefinedBy, catalog_data_url]
  end

  def generate_datasets
    datasets.each do |graph_iri, desc|
      dataset = dataset_iri(graph_iri)
      dataset_data_url = to_data_url(dataset)
      repo << [catalog_iri, RDF::Vocab::DCAT.dataset, dataset ]
      repo << [dataset, RDF.type, RDF::Vocab::DCAT.Dataset ]
      insert_triples language_tagged_literals(dataset, RDF::Vocab::DC.title, desc["title"])
      insert_triples language_tagged_literals(dataset, RDF::Vocab::DC.description, desc["description"])
      repo << [dataset, RDF::Vocab::DC.accrualPeriodicity, RDF::URI.new(desc["frequency"]) ] if desc["frequency"]
      repo << [dataset, RDF::Vocab::RDFS.isDefinedBy, dataset_data_url]
      if desc.key?("themes")
        desc["themes"].each do |theme|
          repo << [dataset, RDF::Vocab::DCAT.theme, RDF::URI.new(theme) ]
        end
      end
      if desc.key?("keywords")
        desc["keywords"].each do |keyword|
          repo << [dataset, RDF::Vocab::DCAT.keyword, RDF::Literal.new(keyword) ]
        end
      end
      repo << [dataset, RDF::Vocab::DCAT.keyword, RDF::Literal.new("linked open data") ]
      repo << [dataset, RDF::Vocab::DCAT.keyword, RDF::Literal.new("sparql") ]
      repo << [dataset, RDF::Vocab::DCAT.keyword, RDF::Literal.new("rdf") ]
      if desc.key?("dump") && desc["dump"].kind_of?(Hash)
        distribution = distribution_iri(dataset, desc["dump"])
        distribution_data_url = to_data_url(distribution)

        dist_modified = check_modified(desc["dump"]["url"])
        repo << [dataset, RDF::Vocab::DCAT.distribution, distribution ]
        repo << [distribution, RDF.type, RDF::Vocab::DCAT.Distribution]
        repo << [distribution, RDF::Vocab::DCAT.downloadURL, RDF::URI.new(desc["dump"]["url"]) ]
        repo << [distribution, RDF::Vocab::DC.format, RDF::Literal.new(desc["dump"]["format"]) ]
        repo << [distribution, RDF::Vocab::DC.license, RDF::URI.new(desc["license"])] if desc["license"]
        repo << [distribution, RDF::Vocab::DC.modified, dist_modified ]
        repo << [distribution, RDF::Vocab::RDFS.isDefinedBy, distribution_data_url]
      end
      distribution = distribution_iri(dataset, {"url": endpoint, "format": "application/sparql-query"})
      distribution_data_url = to_data_url(distribution)
      repo << [dataset, RDF::Vocab::DCAT.distribution, distribution ]
      repo << [distribution, RDF.type, RDF::Vocab::DCAT.Distribution]
      repo << [distribution, RDF::Vocab::DCAT.accessURL, RDF::URI.new(endpoint) ]
      repo << [distribution, RDF::Vocab::DC.format, RDF::Literal.new("application/sparql-query") ]
      repo << [distribution, RDF::Vocab::DC.license, RDF::URI.new(desc["license"])] if desc["license"]
      insert_triples void_triples(distribution, graph_iri)
      repo << [distribution, RDF::Vocab::RDFS.isDefinedBy, distribution_data_url]
    end
  end

  def void_triples(distribution, graph_iri)
    r = []
    stats = VoidStatistics.new(client, graph_iri)
    if stats.triples > 0
      r << [distribution, RDF.type, RDF::Vocab::VOID.Dataset]
      r << [distribution, RDF::Vocab::VOID.sparqlEndpoint, endpoint]
      r << [distribution, RDF::Vocab::VOID.triples, RDF::Literal::Integer.new(stats.triples)]
      r << [distribution, RDF::Vocab::VOID.distinctSubjects, RDF::Literal::Integer.new(stats.subjects)]
      r << [distribution, RDF::Vocab::VOID.properties, RDF::Literal::Integer.new(stats.properties)]
      r << [distribution, RDF::Vocab::VOID.distinctObjects, RDF::Literal::Integer.new(stats.objects)]
      r << [distribution, RDF::Vocab::DC.modified, stats.last_modified]
    end
    r
  end


  def dataset_iri(graph_iri)
    base = catalog_iri.ends_with?('/') ? catalog_iri : "#{catalog_iri}/"
    hash = Digest::SHA256.hexdigest graph_iri
    catalog_iri = RDF::URI.new("#{base}dataset/#{hash}")
  end

  def distribution_iri(dataset_iri, dump)
    hash = Digest::SHA256.hexdigest "#{dump["url"]}#{dump["format"]}"
    RDF::URI.new("#{dataset_iri}/distribution/#{hash}")
  end

  def language_tagged_literals(subject, predicate, lang_map)
    return [] unless lang_map.kind_of?(Hash)
    lang_map.map{ |key, value| RDF::Statement.new(subject, predicate, RDF::Literal.new(value, language: key)) }
  end

  def now
    @now ||= RDF::Literal::DateTime.new(DateTime.now)
  end

  def check_modified(url)
    now
  end
end
